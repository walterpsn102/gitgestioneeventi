CREATE DATABASE IF NOT EXISTS gestioneeventi;

USE gestioneeventi;

CREATE TABLE utenti (
    id_utente INT AUTO_INCREMENT PRIMARY KEY,
    nome VARCHAR(50),
    cognome VARCHAR(50)
);

INSERT INTO utenti (nome, cognome)
VALUES
    ('Mario', 'Rossi'),
    ('Luca', 'Bianchi'),
    ('Giulia', 'Ferrari'),
    ('Paolo', 'Russo'),
    ('Chiara', 'Romano'),
    ('Alessandro', 'Gallo'),
    ('Francesca', 'Conti'),
    ('Marco', 'Marini'),
    ('Simona', 'Greco'),
    ('Giovanni', 'Bruno');

CREATE TABLE organizzatori (
    id_organizzatore INT AUTO_INCREMENT PRIMARY KEY,
    nome VARCHAR(50),
    cognome VARCHAR(50)
);

INSERT INTO organizzatori (nome, cognome)
VALUES
    ('Antonio', 'Esposito'),
    ('Roberta', 'Barbieri'),
    ('Davide', 'Mancini'),
    ('Silvia', 'Pellegrini'),
    ('Fabrizio', 'Santoro');

CREATE TABLE location (
    id_location INT AUTO_INCREMENT PRIMARY KEY,
    luogo VARCHAR(50)
);

INSERT INTO location (luogo)
VALUES
    ('Roma'),
    ('Milano'),
    ('Napoli'),
    ('Firenze'),
    ('Torino');

CREATE TABLE eventi (
    id_evento INT AUTO_INCREMENT PRIMARY KEY,
    id_location INT,
    id_organizzatore INT,
    nome_evento VARCHAR(50),
    data_evento VARCHAR(50),
    FOREIGN KEY (id_location) REFERENCES location(id_location),
    FOREIGN KEY (id_organizzatore) REFERENCES organizzatori(id_organizzatore)
);

INSERT INTO eventi (id_location, id_organizzatore, nome_evento, data_evento)
VALUES
    (1, 1, 'Concerto Rock', '2024-05-10'),
    (2, 2, 'Mostra d\'Arte Moderna', '2024-06-15'),
    (3, 3, 'Sfilata di Moda', '2024-07-20'),
    (4, 4, 'Fiera del Libro', '2024-08-25'),
    (5, 5, 'Festival del Cinema', '2024-09-30');

CREATE TABLE prenotazioni (
    id_prenotazione INT AUTO_INCREMENT PRIMARY KEY,
    id_utente INT,
    id_evento INT,
    FOREIGN KEY (id_utente) REFERENCES utenti(id_utente),
    FOREIGN KEY (id_evento) REFERENCES eventi(id_evento)
);
